# Resilience of transportation infrastructure networks to road failures

## 
Here, we provide the code used for the analysis that we used in the article "Resilience of transportation infrastructure networks to road failures". As the computation for the RoadNetworks that we analysed in this manuscript is quite large, the computational load is quite high. We pre-computed the load values on the cluster and provide the results here. The code for the Figures in the notebooks will therefore not compute the loads, but just load them from files. We provide a My-road-network.ipynb were you can play around with a smaller RoadNetwork, computing everything locally.

# Data and structure

The repo is structured the following way:

```
├── cache/  
├── data/  
│   ├── OSM  
│   ├── GHS  
│   ├── GMP  
│   ├── Copernicus  
├── results/  
│   ├── figs  
│   ├── notebooks  
├── src/  
│   ├── ...
```



## Add data sources
To get the code running, you will need several data sets that you need to download yourself and add to the folder data/.

-data/osmfiles  
On [Geofabrik](http://download.geofabrik.de/europe/germany.html#) navigate to the *.osm.pbf file that is named "germany-210101.osm.pbf", download and put into the folder. The file contains an OSM dump of Germany dated January, 2021. The size of the file is approx. 3.5 GB.

-data/EMSR517  
From [Copernikus](https://emergency.copernicus.eu/mapping/list-of-components/EMSR517) download the latest *.zip file with the title "EMSR517" and unzip it. The data set contains shapefiles of the satelite images after the Ahr flood.

-data/GHSL  
On the [Global Human Settlement Layer](https://ghsl.jrc.ec.europa.eu/download.php?ds=pop) under product select "GHS-POP" and the epoch 2020. Chose the resolution 100m and the Mollweider CRS. Now on the map select the tile R4-C19 to start the download. The *.tif file contains a population raster of the selected area.

-data/GMP  
For the [German Mobility Panel](https://daten.clearingstelle-verkehr.de/order-form.html#192) order the the data set "Mobility Panel Germany (MOP) 1994-2021". The survey results are in a separate folder for each survey year and are available in *.csv format. We need the GMP data for estimation of $\gamma$ (see Figure 2.), as well as for fitting the travel probability function $P(t_{o\rightarrow d})$. You can still run the analysis without the GMP data, as we provide the fit for $P(t_{o\rightarrow d})$ seperately.

## Add paths to the data sources
Before running the code you need to set the paths in the following to the correct files/dirs.

1) In src/RoadNetwork set PATH_TO_OSMFILES = "data/osmfiles/date-of-dump/"
2) In src/FloodModule set PATH_TO_FLOOD_SHAPE = "data/EMSR517flood-shape.shp"
3) In src/PopulationFromRaster set PATH_TO_GHSL_TIF = "data/GHSL/population-raster.tif"
4) In src/GermanMobilityPanel set PATH_TO_GMP = "data/GMP/mobility"
