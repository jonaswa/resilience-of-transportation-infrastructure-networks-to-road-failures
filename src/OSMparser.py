# %%
# ------------------ PACKAGES ---------------------------#
import geopandas as gpd
import os
from pathlib import Path
import hashlib
import osmnx as ox
import pickle
import networkx as nx
import numpy as np
import geopandas as gpd
from shapely.geometry import Polygon

from src import PopulationFromRaster as pfr


# ------------------ FUNCTIONS ---------------------------#
def get_location_shape(location_name):
    """
    Returns the shape of a location in a geopandas dataframe with coordinate reference system epsg:4326.

    Parameters
    ----------
        location_name (str): The name of the location for which to retrieve the shape.

    Returns
    -------
        A geopandas dataframe representing the shape of the location in epsg:4326 coordinate reference system.
    """
    # Use the geopandas library to retrieve the shape of the location
    shape = gpd.read_file(
        f"https://nominatim.openstreetmap.org/search.php?q={location_name}&polygon_geojson=1&format=geojson"
    )

    admin_boundary = shape[shape["type"] == "administrative"]

    # Set the coordinate reference system of the shape to epsg:4326
    admin_boundary = admin_boundary.to_crs("epsg:4326")

    return admin_boundary


def extract_pbf_from_poly(path, osm_pbf, gdf, return_uuid=False):
    """
    Extracts a PBF file from a polygon bounded by a GeoDataFrame.

    Parameters
    ----------
        path (str): The path to the directory where the extracted PBF file and related files will be saved.
        osm_pbf (str): The path to the original OSM PBF file.
        gdf (GeoDataFrame): The GeoDataFrame containing the polygon geometry.
        return_uuid (bool, optional): Whether to return the UUID. Defaults to False.

    Returns
    --------
        str or None: The UUID if `return_uuid` is True, otherwise None.
    """
    uuid = hashlib.sha256(gdf.geometry.iloc[0].wkt.encode("utf-8")).hexdigest()

    if not Path(f"{path}/{uuid}").is_dir():
        os.system(f"mkdir {path}/{uuid}")
        gdf.to_file(f"{path}/{uuid}/polygon.geojson", driver="GeoJSON")

    if not Path(f"{path}/{uuid}/nofilter.osm.pbf").is_file():
        os.system(
            f"osmium extract -p {path}/{uuid}/polygon.geojson {path}/{osm_pbf}\
            -o {path}/{uuid}/nofilter.osm.pbf --overwrite"
        )
    if return_uuid:
        return uuid


def filter_osmpbf(path, osm_pbf, gdf, filter_str, return_uuid=False):
    """
    Filters an OSM PBF file based on a given filter string and the polygon bounded by a GeoDataFrame.

    Paramters
    ---------
        path (str): The path to the directory where the filtered OSM PBF file and related files will be saved.
        osm_pbf (str): The path to the original OSM PBF file.
        gdf (GeoDataFrame): The GeoDataFrame containing the polygon geometry.
        filter_str (str): The filter string to apply to the OSM PBF file.
        return_uuid (bool, optional): Whether to return a unique identifier (uuid) of the Polygon. Defaults to False.

    Returns
    -------
        str or None: The UUID if `return_uuid` is True, otherwise None.
    """
    # Extract the *.osm.pbf bounded by the polygon and create unique identifier str
    uuid = extract_pbf_from_poly(path, osm_pbf, gdf, return_uuid=True)

    filter_hash = hashlib.sha256(filter_str.encode("utf-8")).hexdigest()
    if not Path(f"{path}/{uuid}/{filter_hash}").is_dir():
        os.system(f"mkdir {path}/{uuid}/{filter_hash}")

    if not Path(f"{path}/{uuid}/{filter_hash}/filtered.osm.pbf").is_file():
        os.system(
            f"osmium tags-filter {path}/{uuid}/nofilter.osm.pbf {filter_str}\
            -o {path}/{uuid}/{filter_hash}/filtered.osm.pbf --overwrite"
        )

    # Convert the filtered OSM PBF file to XML format
    osmpbf_to_xml(path, uuid, filter_hash)
    if return_uuid:
        return uuid


def osmpbf_to_xml(path, uuid, filter_hash):
    """
    Converts an OSM PBF file to XML format.

    Parameters
    ----------
        path (str): The path to the directory containing the files.
        uuid (str): The UUID of the polygon.
        filter_hash (str): The hash of the filter string.

    Returns
    -------
        None
    """
    if not Path(f"{path}/{uuid}/{filter_hash}/filtered.osm").is_file():
        os.system(
            f"osmium cat {path}/{uuid}/{filter_hash}/filtered.osm.pbf\
                -o {path}/{uuid}/{filter_hash}/filtered.osm.bz2 --overwrite"
        )
        os.system(f"bzip2 -d {path}/{uuid}/{filter_hash}/filtered.osm.bz2 -f")


def graph_from_gdf(path, osmpbf, gdf, filter_str):
    """
    Creates a networkx graph from a filtered OSM PBF file.

    Parameters
    ----------
        path (str): The path to the directory containing the files.
        osmpbf (str): The path to the original OSM PBF file.
        gdf (GeoDataFrame): The GeoDataFrame containing the polygon geometry.
        filter_str (str): The filter string to apply to the OSM PBF file.

    Returns
    -------
        networkx.MultiDiGraph: The created graph.
    """
    filter_hash = hashlib.sha256(filter_str.encode("utf-8")).hexdigest()
    uuid = filter_osmpbf(path, osmpbf, gdf, filter_str, return_uuid=True)
    G = ox.graph_from_xml(
        f"{path}/{uuid}/{filter_hash}/filtered.osm",
        retain_all=False,
        simplify=True,
    )

    # Remove the temporary files
    os.system(f"rm {path}/{uuid}/{filter_hash}/filtered.osm")
    os.system(f"rm {path}/{uuid}/{filter_hash}/filtered.osm.pbf")
    return G


def gdf_from_bbox(north, south, west, east):
    """
    Creates a GeoDataFrame representing a bounding box defined by coordinates.

    Parameters
    ----------
        north (float): The northern latitude coordinate.
        south (float): The southern latitude coordinate.
        west (float): The western longitude coordinate.
        east (float): The eastern longitude coordinate.

    Returns
    -------
        GeoDataFrame: The GeoDataFrame representing the bounding box.
    """
    # Create a Shapely polygon object using the coordinates
    polygon = Polygon([(west, north), (east, north), (east, south), (west, south)])

    # Create a GeoDataFrame with the polygon as the geometry and CRS set to EPSG:4326
    gdf = gpd.GeoDataFrame(geometry=[polygon], crs="EPSG:4326")
    return gdf


def add_edge_speed(graph):
    """
    Adds edge speeds and travel times to a networkx MultiDiGraph.

    Parameters
    ----------
        graph (MultiDiGraph): The graph to update with edge speeds.

    Returns
    -------
        MultiDiGraph: The updated graph with edge speeds and travel times.
    """
    graph = ox.speed.add_edge_speeds(graph)
    speed_ms_arr = np.array(list(nx.get_edge_attributes(graph, "speed_kph").values()))
    nx.set_edge_attributes(
        graph,
        dict(zip(graph.edges(keys=True), speed_ms_arr * 1000 / 60 / 60)),
        "speed_ms",
    )
    graph = ox.speed.add_edge_travel_times(graph)
    return graph


def road_graph_with_populations(
    return_bound_gdf=False, path=False, osmpbf=False, highway_filter=False, **kwargs
):
    """
    Creates a road graph with population data.

    Parameters
    ----------
        return_bound_gdf (bool, optional): Whether to return the boundary GeoDataFrame. Defaults to False.
        path (str, optional): The path to the directory containing the files. Defaults to False.
        osmpbf (str, optional): The path to the original OSM PBF file. Defaults to False.
        highway_filter (str, optional): The filter string to apply to the OSM PBF file. Defaults to False.
        **kwargs: Additional keyword arguments. Either "place" or "bbox" should be provided.

    Returns
    -------
        tuple or MultiDiGraph: If return_bound_gdf is True, returns a tuple containing the road graph and the boundary GeoDataFrame.
        Otherwise, returns the road graph.
    """
    if "place" in kwargs:
        gdf = get_location_shape(kwargs.get("place"))
    elif "bbox" in kwargs:
        gdf = gdf_from_bbox(*kwargs.get("bbox"))

    uuid = hashlib.sha256(gdf.geometry.iloc[0].wkt.encode("utf-8")).hexdigest()
    filter_hash = hashlib.sha256(highway_filter.encode("utf-8")).hexdigest()

    if not Path(f"{path}/{uuid}/{filter_hash}/graph.gpickle").is_file():
        graph = graph_from_gdf(path, osmpbf, gdf, highway_filter)
        graph = add_edge_speed(graph)

        graph = pfr.population_to_graph_nodes(graph)

        with open(f"{path}/{uuid}/{filter_hash}/graph.gpickle", "wb") as pickle_file:
            pickle.dump(graph, pickle_file)

    else:
        with open(f"{path}/{uuid}/{filter_hash}/graph.gpickle", "rb") as pickle_file:
            graph = pickle.load(pickle_file)
    if return_bound_gdf:
        return graph, gdf

    return graph


# %%
"""


path = "data/osmfiles/latest"
osmpbf = "germany.osm.pbf"
locgdf = get_location_shape("Cologne,Germany")

driving_tags = [
    "motorway",
    "motorway_link",
    "trunk",
    "trunk_link",
    "primary",
    "primary_link",
    "secondary",
    "secondary_link",
    "tertiary",
    "tertiary_link",
    "road",
    "unclassified",
    "residential",
    "living_street",
]

g = graph_from_pbf(path, osmpbf, locgdf, f"w/highway={','.join(driving_tags)}")

nodes, edges = ox.graph_to_gdfs(g)
# %%

edges.plot()
#edges.plot(column=edges["highway"].astype(str), legend=True)

# %%
"""
