# %%
import osmnx as ox
import networkx as nx
import pandas as pd
import geopandas as gpd

# import geopandas as gpd
import numpy as np

# import matplotlib.pyplot as plt
import os
from pathlib import Path
from shapely.strtree import STRtree
import hashlib
import pickle

from src import TrafficCentrality as tc
from src import OSMparser as osmp

# %%


def poi_from_gdf(path, osmpbf, gdf, key, tag):
    """
    Retrieves points of interest (POIs) from a GeoDataFrame based on a specified key and tag.

    Args:
        path (str): The path to the directory to the OSM folder.
        osmpbf (str): The path to the OSM PBF file.
        gdf (geopandas.GeoDataFrame): The GeoDataFrame representing the area of interest.
        key (str): The OSM key for filtering POIs.
        tag (str): The OSM tag for filtering POIs.

    Returns:
        geopandas.GeoDataFrame: A GeoDataFrame containing the retrieved POIs.
    """
    filter_str = f"w/{key}={tag}"
    uuid = hashlib.sha256(gdf.geometry.iloc[0].wkt.encode("utf-8")).hexdigest()

    filter_hash = hashlib.sha256(filter_str.encode("utf-8")).hexdigest()

    if not Path(f"{path}/{uuid}/{filter_hash}/pois.geojson").is_file():
        osmp.filter_osmpbf(path, osmpbf, gdf, filter_str)
        poi_df = ox.geometries_from_xml(f"{path}/{uuid}/{filter_hash}/filtered.osm")
        if len(poi_df) == 0:
            print(f"No {key}_{tag} in specified area.")
            return

        poi_df = poi_df[~poi_df[key].isna()]
        poi_df = poi_df.reset_index().set_index("osmid")
        poi_df = poi_df[[key, "geometry"]]

        poi_df.to_file(f"{path}/{uuid}/{filter_hash}/pois.geojson", driver="GeoJSON")
    else:
        poi_df = gpd.read_file(f"{path}/{uuid}/{filter_hash}/pois.geojson")

    return poi_df


def set_shortest_path_to_emergency(G, weight="travel_time"):
    """
    Extract points of interest (POIs) from a GeoDataFrame based on specified key-value tags using OpenStreetMap data.

    Parameters:
        path (str): The path to the directory to the OSM folder.
        osmpbf (str): Path to the OSM PBF file.
        gdf (geopandas.GeoDataFrame): Input GeoDataFrame representing the area of interest.
        key (str): Key for filtering the POIs.
        tag (str): Tag value for filtering the POIs.

    Returns:
        geopandas.GeoDataFrame: GeoDataFrame containing the extracted POIs.
    """
    nodes = ox.graph_to_gdfs(G, edges=False)
    path_length_emerg = nx.multi_source_dijkstra_path_length(
        G, list(nodes[nodes["emergency"]].index), weight=weight
    )
    path_length_emerg = {k: v / 60 for k, v in path_length_emerg.items()}
    return path_length_emerg


# %%
