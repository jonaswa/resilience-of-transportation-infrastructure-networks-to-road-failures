# %%
# ------------------ PACKAGES ---------------------------#
import osmnx as ox
import networkx as nx
import warnings
import numpy as np
from shapely.strtree import STRtree

from src import PopulationFromRaster as pfr

from src import TrafficCentrality as tc
from src import OSMparser as osmp
from src import EmergencyModule as em
from src import EffectiveVelocities as ev

# %%
# ------------------ SET GLOBALS ---------------------------#

PATH_TO_OSMFILES = "data/osmfiles/2021-01-02"


# %%
# ------------------ FUNCTIONS ---------------------------#
class RoadNetwork:
    """A RoadNetwork that stores a MuliDiGraph, the popuation size per node, as well as the geometries of the nodes and edges.

    Parameters
    ----------
    graph (networkx.Graph): The road network graph.
    bound_gdf (geopandas.GeoDataFrame): A GeoDataFrame representing the boundaries of the road network.
    nodes (geopandas.GeoDataFrame): A GeoDataFrame representing the nodes of the road network.
    edges (geopandas.GeoDataFrame): A GeoDataFrame representing the edges of the road network.
    population (float): The total population of the road network.
    area (float): The area covered by the road network.
    graph_r (None or networkx.Graph): An optional reversed graph of the road network.
    path (str): The path to the OSM files used for constructing the road network.
    osmpbf (str): The path to the OSM PBF file used for constructing the road network.
    """

    def __init__(self, osmpbf=None, highway_filter=None, **kwargs):
        """Initialise a RoadNetwork.

        Parameters
        ----------

        osmpbf (str): The path to the OSM PBF file used for constructing the road network.
        highway_filter (str): OSM filter that is of the form f"w/highway={tag0,tag1,...,tag_N}", where the tags correspond to OSM highway tags.
        **kwargs : keyword arguments. Set to either place="some_place" or bbox="[north,south,west,east]".

        Examples
        --------
        >>> R = rn.RoadNetwork(
                osmpbf="germany.osm.pbf",
                highway_filter=f"w/highway={','.join(["primary","secondary","tertiary"])}",
                bbox=[50.0, 51.2,  6.4, 7.9],
            )
        >>> R = rn.RoadNetwork(
                osmpbf="germany.osm.pbf",
                highway_filter=f"w/highway={','.join(["motorway","trunk","unclassified"])}",
                place="Cologne,Germany",
            )
        """
        self.graph, self.bound_gdf = osmp.road_graph_with_populations(
            return_bound_gdf=True,
            path=PATH_TO_OSMFILES,
            osmpbf=osmpbf,
            highway_filter=highway_filter,
            **kwargs,
        )

        self.nodes, self.edges = ox.graph_to_gdfs(self.graph)
        self.population = self.nodes["population"].sum()
        self.area = (
            self.nodes.set_geometry("voronoi", crs=self.nodes.crs)
            .to_crs("ESRI:54009")
            .area.sum()
            * 1e-6
        )
        self.graph_r = None
        self.osmpbf = osmpbf

        print("num(nodes)=", len(self.graph.nodes()))
        print("num(edges)=", len(self.graph.edges()))

    def loads(self, weight, cache=True, cutoff="default", **kwargs):
        """
        Computes the load (a traffic based centrality measure) for each edge in the graph and sets the values as an edge attribute.

        Parameters
        ----------
        weight  (str) : The edge weight which the shortest path search is based on. Set to 'travel_time' or 'length'.
        cache (bool, optional): Whether to cache results. Defaults to True.
        cutoff (str or int, optional) : The cutoff value when to stop the shortest path search. Defaults to 'default'.
                When set to 'deafult' will only traverse the graph until a maximum shortest path of 3600, if weight='travel_time'
                or to 6000 if weight='length'.
        **kwargs : keyword arguments. Possible options are cpu_cores=N, to run the code in paralell with N cores and
                jobs_per_cpu=M to set the amount of node searches per CPU start. Defaults to 5.
        """
        load_dict = tc.interaction_betweenness_centrality(
            self.graph,
            cache=cache,
            cutoff=cutoff,
            weight=weight,
            return_graph=False,
            **kwargs,
        )
        nx.set_edge_attributes(
            self.graph,
            load_dict,
            "load",
        )
        self.edges["load"] = list(load_dict.values())

    def effective_velocities(self, gamma):
        """Computes the effective velocity of every edge, that is the average traffic affected velocity a vehicle can travel through a road.
        Sets the values as edge attribute.

        Parameters
        ----------
        gamma (str) : The fraction of individuals in a region that are on the road. 0.15 is considered high traffic.

        """
        self.graph = ev.effective_velocities(self.graph, gamma)
        self.nodes, self.edges = ox.graph_to_gdfs(self.graph)

    def effective_spillover_velocities(self, gamma):
        """Reroutes the excess load of every edge to incoming edges and computes the effective velocity of every edge.
        Sets the values as edge attribute.

        Parameters
        ----------
        gamma (str) : The fraction of individuals in a region that are on the road. 0.15 is considered high traffic.

        """
        self.graph = ev.effective_spillover_velocities(self.graph, gamma)
        self.nodes, self.edges = ox.graph_to_gdfs(self.graph)

    def add_pois(self, key, tag):
        """Adds points of interest such as hospitals to graph nodes and sets as node attributes.

        Parameters
        ----------
        key (str) : osm key. Example 'amenity' or 'emergency'
        tag (str) : osm tag. Example 'restaurant' or 'hospital'

        """
        nodes = self.nodes
        poi_gdf = em.poi_from_gdf(
            PATH_TO_OSMFILES, self.osmpbf, self.bound_gdf, key, tag
        )

        # set true/false for poi nodes
        if key not in nodes.columns:
            nodes[key] = np.full(len(nodes), False)

        geoms = poi_gdf.geometry
        node_tree = STRtree(nodes.geometry)

        for j, geo in enumerate(geoms):
            # find nearest emegreny to road i
            centroid_pt = geo.centroid
            idx = node_tree.nearest(centroid_pt)

            # set poi to ndoe
            nodes.iloc[idx, nodes.columns.get_loc(key)] = poi_gdf[key].iloc[j]

        nx.set_node_attributes(self.graph, nodes[key], name=key)

    def remove_edges(self, edges):
        """Effectively removes edge from graph, by setting the speed limit to zero, the travel time to inf and the length to inf,
        ensuring that the shortest path algorithm will never visit this edge.

        Parameters
        ----------
        edges (iterator) : Iterator for edges, e.g. list of edge indices.
        """
        graph_r = self.graph.copy()
        for e in edges:
            u, v, k = e
            graph_r[u][v][k]["speed_kph"] = 0
            graph_r[u][v][k]["speed_ms"] = 0
            graph_r[u][v][k]["maxspeed"] = 0
            graph_r[u][v][k]["travel_time"] = np.inf
            graph_r[u][v][k]["length"] = np.inf
            graph_r[u][v][k]["removed"] = True
        self.graph_r = graph_r


# %%


def remove_edges(graph, edg):
    graph_r = graph.copy()
    for e in edg:
        u, v, k = e
        graph_r[u][v][k]["speed_kph"] = 0
        graph_r[u][v][k]["speed_ms"] = 0
        graph_r[u][v][k]["maxspeed"] = 0
        graph_r[u][v][k]["travel_time"] = np.inf
        graph_r[u][v][k]["length"] = np.inf
        graph_r[u][v][k]["removed"] = True
    return graph_r


def convert_road_to_bicycle_lane(graph, edg):
    graph_r = graph.copy()
    for e in edg:
        u, v, k = e
        graph_r[u][v][k]["speed_kph"] = 5
        graph_r[u][v][k]["maxspeed"] = 5
        graph_r[u][v][k]["speed_ms"] = 5 * 1000 / 60 / 60
        graph_r[u][v][k]["travel_time"] = (
            graph_r[u][v][k]["length"] / graph_r[u][v][k]["speed_ms"]
        )
        graph_r[u][v][k]["bike_lane"] = True

    return graph_r


"""
# %%
west, east = 6.4, 7.9
south, north = 50.0, 51.2

driving_tags = [
    "motorway",
    "motorway_link",
    "trunk",
    "trunk_link",
    "primary",
    "primary_link",
    "secondary",
    "secondary_link",
    "tertiary",
    "tertiary_link",
]
highway_filter = f"w/highway={','.join(driving_tags)}"

place = RoadNetwork(
    osmpbf="germany.osm.pbf",
    highway_filter=highway_filter,
    bbox=[north, south, west, east],
)

# %%
"""
